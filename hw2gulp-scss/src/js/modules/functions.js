export function isWebp() {

   function testWebP() {
      let webP = new Image();
      
      webP.onload = webP.onerror = function () {
         return (webP.height == 2);
      };
      webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
   }
   
   testWebP(function (support) {
      let className = support === true ? 'webp' : 'no-webp';
      document.documentElement.classList.add(className);
   });
}

export function addClass () {
   
   const burgerIcon = document.querySelector(".top-nav__icon");
   const burgerList = document.querySelector(".top-nav__list");
   const burgerListShow = document.querySelector(".top-nav__link");


   burgerIcon.addEventListener("click", (event) => {
      burgerIcon.classList.toggle('opened');
      if (burgerIcon.classList.contains('opened')) {
         burgerList.classList += '--show';
         

      } else {
         
         burgerList.classList = 'top-nav__list';
      }


   });

   window.addEventListener('resize', (event) => {
      let screenWidth = document.documentElement.clientWidth;
      if (screenWidth >= 768) {
         burgerIcon.classList.remove('opened');
         burgerList.classList = 'top-nav__list';
      }
      
   })


 

}