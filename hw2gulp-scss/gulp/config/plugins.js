import replace from "gulp-replace";       // Пошук та заміна
import plumber from "gulp-plumber";       // Обробка помилок
import notify from "gulp-notify";         // Повідомлення (підказки)
import browserSync from "browser-sync";   // Локальний сервер
import newer from "gulp-newer";           // Перевірка оновлення
import ifPlugin from "gulp-if";           // Умова розгалуження

// Експорт об'єкта
export const plugins = {
   replace: replace,
   plumber: plumber,
   notify: notify,
   browserSync: browserSync,
   newer: newer,
   if: ifPlugin
}