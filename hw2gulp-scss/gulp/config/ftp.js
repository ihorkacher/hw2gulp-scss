export let configFTP = {
   host: "",      // Адреса ftp сервера
   user: "",      // Ім'я користувача
   password: "",  // Пароль
   parallel: 5    // Кількість одночасних потоків
}